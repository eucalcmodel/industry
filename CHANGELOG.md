# Changelog


## Unreleased

### Added
- biogenic CO2 quantity

### Changed
- energy carrier switch calculation
- calibration year: material production from 1990 to 2006 in order to prevent glitch caused in buildings

### Removed

## 2.6

### Added
- cost calculation for carbon capture
- opex calculation

### Changed
- interface with employment, minerals
- naming convention buuildings
- optimised nodes (merge groups)

### Removed
- energy carrier mix (temporarily)

## 2.1

### Added
- calibration node JRC IDEES on energy
- calibration node EUROSTAT on emissions
- interface air pollution

### Changed
- calibration of production, energy and emissions

### Removed


## 1.9

### Added
- calibration node JRC IDEES on energy, implementation started not completed
- calibration node EUROSTAT on emissions, implementation started not completed

### Changed
- Update interfaces ammonia and industry to water, power, employment
- add historic time series for non-energy intensive industries
- calibration of production, energy and emissions

### Removed
- CO2e emissions from TPE


## 1.8

### Added
- added copper as a new material

### Changed
- bugs in code (math formulas)

### Removed

## 1.7

### Added
- added lfs products (aluminium and glass packaging)
- added non-energy intensive sectors

### Changed
- regex emission factors
- update L2 interface climate and employment

### Removed


## 1.6

### Added
- interface with climate
- added materials aluminium, glass, lime
- interface with employment

### Changed

### Removed
- missing data boxes from previous release


## 1.5

### Added
- cost metanode (industry and ammonia)
- new products added in interface with buildings

### Changed
- import values for products and materials changed (coming from WP7)

### Removed


## 1.4
### Added
- new output for GTAP
- interface with district heating
- interface with minerals
- interface with CCUS (from ammonia)
### Changed

### Removed
- electricity used for CC

## 1.3
### Added
- New interface with CCUS module
- Calibration of timber production
- Calculation of CH4 and N2O in industry
- Calculation of CH4 and N2O in ammonia
- Interface with water module (industry and ammonia)
- Historical data for ammonia production (1995-2015)
- Boundary values for output validation
### Changes
- Update of md files
- Update of output validation
- New interface of industry and ammonia with  the pathways explorer
- Naming of output to PE following the official one (metrics and vectors)
### Removes

## 1.1
### Added
- Visualization of outputs divided by industrial sector 
- Added the matrial switch ratios in the calculations of material switch in products
- Added a new lever called 'material net import'
- node that replace +inf and -inf with zero in the calculation of the net import percentage of products (result of x/0 division)
- Added a new lever called 'product net import'
- Values of net import % provided by WP7 for materials and products
- Starting to add the box documentation (description, inputs, outputs)
- Added values for 2015 in ots file of energy carrier mix
- Added granularity in biomass demand output for the Agriculture module
- Documentation box for industry node finished 
- Added calibration for production, energy and emissions
### Changes
- 'Energy vector mix' lever is now called 'energy carrier mix'
- The technology 'DRI-EAF' has been replaced with the technology 'hydrog-DRI' (for steel)
- The ammonia workflow (3.1b Industry ammonia node) has been updated (python nodes deleted and constants coming from the technology module)
- Update of net import lever with percentage 
- S-curve for projections of material intensity and material switch
- Calibration of production using the node provided by climact
- Lever 'material-intensity' changed in 'material-efficiency'
- Lever 'CCS' changed in 'CC'
### Removes
### Fixed 
- OTS real values for 2015

## 0.9
- Initial version, integrating all modules